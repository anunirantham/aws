What is AWS Certification
Amazon Web Service is a cloud platform for those people who can show their skills through various AWS certifications .it is trusted by many firms because of the features it provides.
Types of aws certifications
There are 6 types of certifications in AWS. They are
1.Cloud practitioner path
this path is for individuals who work with aws cloud in technical, sales, purchasing, or financial roles.
2.Architect path
This path is for solution architect. Anyone who wants to learn how to build applications and systems on aws can choose this path.there are two subpaths in this architect path.
     a.aws certified solution architect-Associate
           for this path of certification the student must have the knowledge of designing, managing, and distributing of applications in conjunction with aws tools.in this certification the students must have one year of experience in designing.
     b.aws certified solution architect-Professional                   
            for this certification the student must have completed the associate level in aws certified solution architect and should be 2 or more years experienced in aws cloud architectures.
3.developer path
This path is for those who want to learn how to develop cloud applications on aws cloud

Join [AWS Training Online](https://intellipaat.com/artificial-intelligence-deep-learning-course-with-tensorflow/) to learn more about AWS certifications

This path consists of two sub-paths
     a.aws certified developer-associate
it requires code-level knowledge for designing application, maintenance, and development.must have the knowledge of aws architecture. this path requires more than one year of experience in maintaining the aws applications.
     b.aws certified developer –professional
It requires the applicant to complete the  associate level in aws certified developer and must have more than two years of experience in providing and managing aws architecture.
4.operations path
  This path is for those who want to learn how to create automatable and repeatable deployments of applications, networks, and systems on the aws platform.
This path consists of two sub-paths
   a.aws certified SysOps administrator-Associate
it provides the knowledge about deployment and operations related to aws architecture and services. the applicant must possess the basic skills of the system administrator.
       b.aws certified DevOps-professional
            it requires the applicant to complete the associate level in aws certified SysOps administrator and must have more than two years of experience in providing and managing aws architecture.
5.Specialty path
It offers five specialty certifications in technical areas.
a.aws certified big data
            requires one associative level certification and five years of experience in data analytics and background in aws big data tools. Student must confirm technical and experiment skills in big data architecture design
      b.aws certified  advanced networking
            requires at least one associative level certification and five years of experience in data analytics and background in aws networking student must possess an advanced level of knowledge in aws tools and services
     c.aws certified security
           it is for individuals who perform security roles with a minimum of two years of experience.
   d.aws certified machine learning
            this can be used by developers, data scientists, data platform engineers and business decision-makers to learn how to use them in their businesses.
   e.aws certified Alexa skill-building
            it is for those individuals who play the role of Alexa skill-building.it validates a candidates ability of building, testing, and publishing Amazon Alexa skills. Certificate should be able to:
Explain the value of voice
Design the user experience
Design the architecture to build the skills
Manage skill operations and life cycles.
Aws features
Amazon web service has many features which make it more popular among other firms
1.moblie friendly access
Aws features are both for android and ios. AWS mobile hub helps you in guiding the best feature for your app.
2. Serverless cloud functions
Amazon API and amazon gateway helps users by running their code and scaling it.it helps the user in such a way that they should focus only on application building.
3.data bases
Amazon provides them databases that are required and they are completely manageable.


4.storage
The storage provided by aws can be used individually or combined to meet your requirements.the storage provided by aws is flexible and easy to use.
How to get certified 
Associate-level exam focuses on the following aws areas.
Amazon EC2
Amazon S3
Amazon VPC
Amazon route 53
Amazon relational database service
  
